import { src, dest, parallel, watch } from 'gulp';
import htmlmin from 'gulp-htmlmin';
import jsonMinify from 'gulp-json-minify';
import gulpSass from 'gulp-sass';

const path = {
  sass: 'src/style/index.scss',
  dest: './dist',
  base: 'src/',
  html: 'src/**/*.html',
  json: 'src/**/*.json',
};

const html = () => src(path.html, { base: path.base }).pipe(dest(path.dest));

const json = () => src(path.json, { base: path.base }).pipe(dest(path.dest));

const sass = () =>
  src(path.sass)
    .pipe(gulpSass())
    .pipe(dest(path.dest));

export const copy = parallel(html, json, sass);

const watchHtml = () => watch(path.html, html);

const watchJson = () => watch(path.json, json);

const watchSass = () => watch('src/style/*.scss', sass);

const watchGulp = parallel(watchHtml, watchJson, watchSass);

export default watchGulp;

const minifyJson = () =>
  src(path.json, { base: path.base })
    .pipe(jsonMinify())
    .pipe(dest(path.dest));

const minifyHtml = () =>
  src(path.html, { base: path.base })
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(dest(path.dest));

const miniSass = () =>
  src(path.sass)
    .pipe(gulpSass({ outputStyle: 'compressed' }))
    .pipe(dest(path.dest));

export const minify = parallel(minifyJson, minifyHtml, miniSass);
