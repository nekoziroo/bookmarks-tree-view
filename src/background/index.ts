const openBookmarksTreeView = () => {
  browser.tabs.create({
    active: true,
    url: 'tree-view/index.html',
  });
};

browser.browserAction.onClicked.addListener(openBookmarksTreeView);
