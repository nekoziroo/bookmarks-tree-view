import * as React from 'react';
import bookmark from './Bookmark';
import Bookmark from './Bookmark';

interface Props {
  tree: browser.bookmarks.BookmarkTreeNode[];
  root?: boolean;
}

const Branch: React.SFC<Props> = ({ tree, root }): JSX.Element => {
  const treeList = tree.map(tree => {
    if (tree.type === 'folder') {
      return (
        <details key={tree.index} className={`folder ${root ? 'root' : 'branch'}`}>
          <summary>{tree.title}</summary>
          <Branch tree={tree.children} />
        </details>
      );
    }

    if (tree.type === 'bookmark') {
      return <Bookmark tree={tree} root={root} />;
    }
  });

  return <React.Fragment>{treeList}</React.Fragment>;
};

export default Branch;
