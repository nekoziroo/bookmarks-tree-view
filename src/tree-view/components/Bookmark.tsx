import * as React from 'react';

interface Props {
  tree: browser.bookmarks.BookmarkTreeNode;
  root?: boolean;
}

const Bookmark: React.SFC<Props> = ({ tree, root }) => {
  return (
    <div key={tree.index}>
      <a className={root ? 'root' : 'branch'} href={tree.url}>
        {tree.title}
      </a>
    </div>
  );
};

export default Bookmark;
