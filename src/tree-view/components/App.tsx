import * as React from 'react';
import Branch from './Branch';

interface State {
  tree: browser.bookmarks.BookmarkTreeNode[];
}

class App extends React.Component<{}, State> {
  constructor({}) {
    super({});
    this.state = {
      tree: [],
    };
  }
  public async componentDidMount(): Promise<void> {
    const bookmark = await browser.bookmarks.getTree();
    this.setState({
      tree: bookmark[0].children,
    });
  }

  public render(): JSX.Element {
    return (
      <div>
        <Branch tree={this.state.tree} root={true} />
      </div>
    );
  }
}

export default App;
